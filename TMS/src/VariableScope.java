/**
 * Local variables scope is within the block. Instance variables remains until
 * the garbage collection. Class variables are until the program ends.
 *
 */
public class VariableScope {
	static int Max_LENGTH = 5;
	int length;
	int Public;

	public VariableScope(int length, int public1) {
		super();
		this.length = length;
		Public = public1;
	}

	public static int getMax_LENGTH() {
		return Max_LENGTH;
	}

	public static void setMax_LENGTH(int max_LENGTH) {
		Max_LENGTH = max_LENGTH;
	}

	public int getLength() {
		return length;
	}

	public void setLength(int length) {
		this.length = length;
	}

	public int getPublic() {
		return Public;
	}

	public void setPublic(int public1) {
		Public = public1;
	}

	public void grow(int inches) {
		if (length < Max_LENGTH) {
			int newSize = length + inches;
			length = newSize;
		}
	}
}
