
/**
 * @Order of Initialisation :Blocks first and then Constructors number can have
 *        _ in zeros. Primitives cannot be assigned to null but reference can.
 *        reference can called method on them when not assigned to null but
 *        primitive cannot. primitive types are lower case but reference are
 *        upper case.
 */
public class Chick {

	private String name = "Fluffy";

	int a = 1_000_000;
	float f = 1_000.0_00f;
	long l = 3244444456L;

	{
		System.out.println("setting field");
	}

	public Chick() {
		name = "Tiny";
		System.out.println("setting constructor");
	}

	/*
	 * local variable must be initialised before using it-
	 */
	public int notValid() {
		int y = 10;
		int x = 11;
		int z = x + y;
		return z;
	}

	public static void main(String[] args) {
		Chick obj = new Chick();
		System.out.println(obj.name);
	}
}
